"""
Marvel api connector module
"""
from _md5 import md5
from random import randint

import requests
from django.utils import timezone
from django.conf import settings


class MarvelAPIConnectorException(Exception):
    """
    Custom exception for marvel connector
    """
    pass


class MarvelAPIConnector(object):
    """
    Connector class for marvel API
    """
    def __init__(self):
        """
        Set up params
        """
        self.ok_status_code = 200
        try:
            self.private_key = settings.PRIVATE_KEY
            self.public_key = settings.PUBLIC_KEY
        except AttributeError:
            raise MarvelAPIConnectorException(
                "PRIVATE_KEY and PUBLIC_KEY settings are missed")
        self.timestamp = int(timezone.now().timestamp())
        self.string_to_hash = "{}{}{}".format(
            self.timestamp, self.private_key, self.public_key)
        self.hash = md5(self.string_to_hash.encode()).hexdigest()
        self.api_version = "v1"
        self.api_url = "https://gateway.marvel.com/{}/public/{}"

    def __set_up_url(self, recource):
        """
        Build api url and add authorization info into it
        """
        url = self.api_url.format(self.api_version, recource)
        url = "{}?ts={}&apikey={}&hash={}".format(
            url, self.timestamp, self.public_key, self.hash)
        return url

    def __get_headers(self):
        """
        Prepare request headers
        """
        return {"Accept": "application/json"}

    def __execute_get_call(self, url):
        """
        Execute GET call to marvel api
        """
        try:
            response = requests.get(url, headers=self.__get_headers())
        except Exception as e:
            raise MarvelAPIConnectorException(
                "Unable to reach Marvel API."
                " Original exception is: {}".format(e))
        if response.status_code > self.ok_status_code:
            raise MarvelAPIConnectorException(
                "Error during marvel API call, status code: {}".format(
                    response.status_code))
        return response.json()

    def __search_character(self, character_name):
        """
        Search character by specified name and obtain its id
        """
        resource = "characters"
        limit = 1
        url = self.__set_up_url(resource)
        url = "{}&nameStartsWith={}&limit={}".format(
            url, character_name, limit)
        response_data = self.__execute_get_call(url)
        results = response_data.get("data").get("results")
        if not results:
            return None
        character_id = results[0].get("id")
        return character_id

    def __obtain_character_image(self, character_id):
        """
        Obtain character image url
        """
        image_postfix = "/landscape_xlarge.jpg"
        resource = "characters/{}".format(character_id)
        url = self.__set_up_url(resource)
        response_data = self.__execute_get_call(url)
        image_url = response_data.get(
            "data").get("results")[0].get("thumbnail").get("path")
        image_url = "{}{}".format(image_url, image_postfix)
        return image_url

    def get_story(self, character_name):
        """
        Get character story
        """
        data = {
            "title": None,
            "description": None,
            "characters": []
        }
        limit = 100
        character_id = self.__search_character(character_name)
        resource = "characters/{}/stories".format(character_id)
        url = self.__set_up_url(resource)
        url = "{}&characters={}&limit={}".format(url, character_id, limit)
        response_data = self.__execute_get_call(url)
        results = response_data.get("data").get("results")
        if not results:
            return data
        story = results[randint(0, len(results) - 1)]
        data["attribution_text"] = response_data.get("attributionText")
        data["title"] = story.get("title")
        data["description"] = story.get("description")
        data["characters"] = [
                {"name": character.get("name"),
                 "image": self.__obtain_character_image(
                     character.get("resourceURI").split("/")[-1])}
                for character in story.get("characters").get("items")]
        return data
