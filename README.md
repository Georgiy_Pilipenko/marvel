# Marvel 1.0
## Heorhii Pylypenko

### Installation
1. **Clone project:**
    `git clone https://Georgiy_Pilipenko@bitbucket.org/Georgiy_Pilipenko/marvel.git`
2. **Install requirements:**
    `pip install -r requirements.txt`
3. **Set `PRIVATE_KEY` and `PUBLIC_KEY` variables using appropriate keys from your marvel api developer account in `setting.py`** 

### Basic usage
1. **Run server:**
    `python manage.py runserver`
2. **Open home page in your browser:**
    `http://127.0.0.1:8000/`
3. **To obtain another random story simply reload the page.**

### Optional
You can extend `MARVEL_CHARACTERS_NAMES` variable in `setting.py` to obtain random stories for extra characters.

Currently project picks a random character from predefined list and random story for that character.


