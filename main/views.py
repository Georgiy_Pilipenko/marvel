"""
Main app views module
"""
from random import randint

from django.conf import settings
from django.template.response import TemplateResponse
from django.views import View

from utils.marvel_api import MarvelAPIConnector, MarvelAPIConnectorException


class Main(View):
    """
    Main view
    """
    template_name = "base.html"

    def get(self, request):
        """
        Extend get procedure
        """
        character_name = settings.MARVEL_CHARACTERS_NAMES[
            randint(0, len(settings.MARVEL_CHARACTERS_NAMES) - 1)]
        try:
            marvel_api_connector = MarvelAPIConnector()
            story = marvel_api_connector.get_story(character_name)
        except MarvelAPIConnectorException:
            return TemplateResponse(
                request, self.template_name, context={"error": True})
        return TemplateResponse(request, self.template_name, context=story)
