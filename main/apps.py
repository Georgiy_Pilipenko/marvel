"""
Main app config module
"""
from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'main'
