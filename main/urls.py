"""
Main application urls module
"""
from django.conf.urls import url

from main.views import Main

urlpatterns = [
    url(r'^', Main.as_view()),
]
