"""
Main urls module
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from main import urls as main_urls

urlpatterns = [
    url(r'^', include(main_urls, namespace="mainr_api_urls")),
]
# ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

